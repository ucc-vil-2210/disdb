import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Producto {
    private int id;
    private String nombre;
    private double precio;
    private int existencia;
    public Producto(int id, String nombre, double precio, int existencia) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.existencia = existencia;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public int getExistencia() {
        return existencia;
    }
    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }
    @Override
    public String toString() {
        return "Producto [existencia=" + existencia + ", id=" + id + ", nombre=" + nombre + ", precio=" + precio + "]";
    }
    
    public void guardarBD() {
        try {
            Conexion con = new Conexion();
            String SQL;
            SQL="INSERT INTO Producto (id, nombre, precio, existencia) VALUES (?,?,?,?)";
            con.conectar();
            //System.out.println("SQL :"+SQL);
            if (con.getConexion() !=null)
            {
                PreparedStatement st = con.getConexion().prepareStatement(SQL);
                //st.setDouble(1, emp.getId());
                st.setInt(1, this.id);
                st.setString(2,this.nombre);
                st.setDouble(3, this.precio);
                st.setInt(4,this.existencia);
                st.executeUpdate();
                con.cerrarConexion();
                System.out.printf("Producto creado: %d, nombre: %s, precio: %d, existencias: %d\n", 
                                        this.id, this.nombre, this.precio, this.existencia );
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public static  ArrayList<Producto> leer() {
        ArrayList<Producto> ans = new ArrayList();
        ResultSet listado = null;
        try {
            Conexion bd = new Conexion();
            String SQL="SELECT id, nombre, precio, existencia FROM Producto;";
            bd.conectar();
            PreparedStatement st = bd.getConexion().prepareStatement(SQL);
            listado = st.executeQuery();
            int total = 0;
            while (listado.next()){
                ans.add(new Producto(listado.getInt("id"), listado.getString("nombre"), listado.getDouble("precio"), listado.getInt("existencia")));
               //Obtienes la data que necesitas...
               total++;
            }
            System.out.println("Cantidad de Productos es: "+(total));
            bd.cerrarConexion();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return ans;
    }
}
