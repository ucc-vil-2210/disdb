public class FormaDePago {
    private int id;
    private String nombre;
    public FormaDePago(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    @Override
    public String toString() {
        return "FormaDePago [id=" + id + ", nombre=" + nombre + "]";
    }
    
}
