import java.util.Date;

public class Pedido {
    private int id;
    private Date fecha;
    private int id_cliente;
    private int id_forma_pago;
    public Pedido(int id, Date fecha, int id_cliente, int id_forma_pago) {
        this.id = id;
        this.fecha = fecha;
        this.id_cliente = id_cliente;
        this.id_forma_pago = id_forma_pago;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Date getFecha() {
        return fecha;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public int getId_cliente() {
        return id_cliente;
    }
    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }
    public int getId_forma_pago() {
        return id_forma_pago;
    }
    public void setId_forma_pago(int id_forma_pago) {
        this.id_forma_pago = id_forma_pago;
    }
    @Override
    public String toString() {
        return "Pedido [fecha=" + fecha + ", id=" + id + ", id_cliente=" + id_cliente + ", id_forma_pago="
                + id_forma_pago + "]";
    }
}
