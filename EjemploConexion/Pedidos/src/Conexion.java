import java.sql.Connection;         // Se conecta
import java.sql.DriverManager;      // Getiona el Driver de la base de datos
import java.sql.PreparedStatement;  // Permite ejecutar instrucciones SQL sobre la base de datos
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {
    private String url = "jdbc:mysql://localhost:3306/mydb?serverTimezone=UTC";
    private Connection conexion;
    private String username = "root";
    private String password = "-a123456";


    public void conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection(url, username, password);

            // Class.forName("org.sqlite.JDBC");
            // conexion = DriverManager.getConnection("jdbc:sqlite:"+url);
            if (conexion!=null) {
                System.out.println("Conectado");
            }
        }catch (SQLException ex) {
            System.err.println("No se ha podido conectar a la base de datos\n"+ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cerrarConexion(){
           try {
               conexion.close();
           } catch (SQLException ex) {
               System.err.println("No se ha podido cerrar la conexion a la base de datos\n"+ex.getMessage());
           }
    }
    

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public Connection getConexion() {
        return conexion;
    }
    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
