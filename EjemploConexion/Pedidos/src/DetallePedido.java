public class DetallePedido {
    private int sec;
    private int cantidad;
    private double precio;
    private int id_pedido;
    private int id_producto;
    public DetallePedido(int sec, int cantidad, double precio, int id_pedido, int id_producto) {
        this.sec = sec;
        this.cantidad = cantidad;
        this.precio = precio;
        this.id_pedido = id_pedido;
        this.id_producto = id_producto;
    }
    public int getSec() {
        return sec;
    }
    public void setSec(int sec) {
        this.sec = sec;
    }
    public int getCantidad() {
        return cantidad;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public int getId_pedido() {
        return id_pedido;
    }
    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }
    public int getId_producto() {
        return id_producto;
    }
    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }
    @Override
    public String toString() {
        return "DetallePedido [cantidad=" + cantidad + ", id_pedido=" + id_pedido + ", id_producto=" + id_producto
                + ", precio=" + precio + ", sec=" + sec + "]";
    }
}
