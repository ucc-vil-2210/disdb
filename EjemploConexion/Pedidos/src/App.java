public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        try {
            Conexion con = new Conexion();
            con.conectar();
            
            con.cerrarConexion();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
}
